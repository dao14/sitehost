<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Customer 293785</title>

    <body>
    	<?php
		$url = 'https://api.demo.sitehost.co.nz'
		$data = [
			'collection' => 'RapidAPI'
		];

		$curl = curl_init( $url );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true ) ;
		curl_setopt( $curl, CURLOPT_POST, true ) ;
		curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode( $data ) ) ;
		curl_setopt( $curl, CURLOPT_HTTPHEADER, [
			'X-RapidAPI-Host: api.demo.sitehost.co.nz',
			'X-RapidAPI-Key: d17261d51ff7046b760bd95b4ce781ac',
			'Content-TYpe: application/json'
		] ) ;

		$response = curl_exec( $curl ) ;
		curl_close( $curl ) ;
		echo $response . PHP_EOL ;
	?>
    </body>
</html>
